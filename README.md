## VVapp ##

# What? #
An app to help managing passage order on the differents riding bikes and memorising laps time.

# For whom? #
Every scout troup of belgium, participating to the "24h v�los du bois de la cambre" event.

# How to contribute? #
This project will probably not be continued, another one should replace it.  It is surely not perfect, there are still some bugs to fix.  The best way to contribute is to share a better version of this project, on your own repo.

# How can I get it? #
This app isn't available on the Play Store, and never will be.  To enjoy using it, you just need to clone the project, setup a Firebase account and share it to your troup members.

# Firebase #
Firebase is a group a tools developped by Google to help you manage project with server requirement without even care about the server part...  Please check this link for more infos : https://firebase.google.com/
In 2018, this app has been used with the free pricing plan of Firebase and success to withstand the charge of 22 active users.  It should be able to go up to 30 users, for more you should take another pricing plan.

# One last thing #
This is an Android app, sorry dear Apple users.