package com.muntjac.wl042.muntjac.activities;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.fragments.BikesFragment;
import com.muntjac.wl042.muntjac.fragments.MenuFragment;
import com.muntjac.wl042.muntjac.fragments.UsersFragment;
import com.muntjac.wl042.muntjac.java.Bike;
import com.muntjac.wl042.muntjac.java.Constants;
import com.muntjac.wl042.muntjac.java.Lap;
import com.muntjac.wl042.muntjac.java.User;

import java.util.ArrayList;
import java.util.Collections;

import static com.muntjac.wl042.muntjac.java.Constants.db;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "MainActivity";

    public static FirebaseUser currentUser = null;
    public static ArrayList<Bike> myBikes;
    public static ArrayList<User> users;
    public static User user = null;

    public static boolean onMenu = true;

    public static Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setUpMain();
    }

    private void setUpMain() {
        myBikes = new ArrayList<>();
        users = new ArrayList<>();
        user = null;

        /* Default fragment */
        Fragment fragment = new MenuFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

        db.collection(Constants.COLLECTION_USERS).document(currentUser.getEmail())
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    if(doc != null && doc.exists()) {
                        user = doc.toObject(User.class);
                        user.setID(doc.getId());
                    }
                    else db.collection(Constants.COLLECTION_USERS)
                            .document(currentUser.getEmail())
                            .set(new User(currentUser.getDisplayName(), false));
                }
                else {
                    db.collection(Constants.COLLECTION_USERS)
                            .document(currentUser.getEmail())
                            .set(new User(currentUser.getDisplayName(), false));
                }
            }
        });

        createBikesListener();
        createUsersListener();
    }

    private void createBikesListener() {
        myBikes.clear();
        db.collection(Constants.COLLECTION_BIKES).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                if(documentSnapshots == null) return;
                for(DocumentChange change : documentSnapshots.getDocumentChanges()) {
                    Bike bike = change.getDocument().toObject(Bike.class);
                    bike.setID(change.getDocument().getId());
                    int position = Collections.binarySearch(myBikes, bike);
                    switch(change.getType()) {
                        case ADDED :
                            myBikes.add(bike);
                            Collections.sort(myBikes);
                            createLapsListener(bike);
                            if(MenuFragment.mHandler != null) {
                                Message message = MenuFragment.mHandler.obtainMessage(MenuFragment.BIKES, bike.getID());
                                message.sendToTarget();
                            }
                            break;
                        case MODIFIED :
                            if(position >= 0) {
                                myBikes.get(position).update(bike);
                                if(BikesFragment.mHandler != null) {
                                    Message message = BikesFragment.mHandler.obtainMessage(BikesFragment.MODIFIED, bike.getID());
                                    message.sendToTarget();
                                }
                                if(MenuFragment.mHandler != null) {
                                    Message message = MenuFragment.mHandler.obtainMessage(MenuFragment.BIKES, bike.getID());
                                    message.sendToTarget();
                                }
                            }
                            break;
                        case REMOVED :
                            myBikes.remove(position);
                            if(BikesFragment.mHandler != null) {
                                Message message = BikesFragment.mHandler.obtainMessage(BikesFragment.REMOVED, bike.getID());
                                message.sendToTarget();
                            }
                            if(MenuFragment.mHandler != null) {
                                Message message = MenuFragment.mHandler.obtainMessage(MenuFragment.BIKES, bike.getID());
                                message.sendToTarget();
                            }
                            break;
                    }
                }
            }
        });
    }

    private void createLapsListener(final Bike bike) {
        db.collection(Constants.COLLECTION_BIKES)
                .document(bike.getID())
                .collection(Constants.COLLECTION_DONE)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if(documentSnapshots == null) return;
                        for(DocumentChange change : documentSnapshots.getDocumentChanges()) {
                            Lap lap = change.getDocument().toObject(Lap.class);
                            lap.setID(change.getDocument().getId());
                            int position = Collections.binarySearch(bike.getDone(), lap);
                            switch(change.getType()) {
                                case ADDED : bike.getDone().add(lap);
                                    Collections.sort(bike.getDone());
                                    break;
                                case MODIFIED : if(position >= 0) bike.getDone().get(position).update(lap);
                                    break;
                                case REMOVED :
                                    bike.getDone().remove(position);
                                    break;
                            }
                            if(BikesFragment.mHandler != null) {
                                Message message = BikesFragment.mHandler.obtainMessage(BikesFragment.MODIFIED, bike.getID());
                                message.sendToTarget();
                            }
                        }
                    }
                });
    }

    private void createUsersListener() {
        Constants.db.collection(Constants.COLLECTION_USERS)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if(documentSnapshots == null) return;
                        for(DocumentChange change : documentSnapshots.getDocumentChanges()) {
                            User user = change.getDocument().toObject(User.class);
                            user.setID(change.getDocument().getId());
                            int position = Collections.binarySearch(users, user);
                            switch (change.getType()) {
                                case ADDED : users.add(user);
                                    Collections.sort(users);
                                    if(UsersFragment.mHandler != null) {
                                        Message message = UsersFragment.mHandler.obtainMessage(UsersFragment.UPDATE);
                                        message.sendToTarget();
                                    }
                                    if(currentUser!= null && user.getID().equals(currentUser.getEmail())) {
                                        if(MainActivity.user == null) MainActivity.user = user;
                                        else MainActivity.user.update(user);
                                        notifyPermChanged();
                                    }
                                    break;
                                case MODIFIED : if(position >= 0) users.get(position).update(user);
                                    if(currentUser!= null && user.getID().equals(currentUser.getEmail())) {
                                        if(MainActivity.user == null) MainActivity.user = user;
                                        else MainActivity.user.update(user);
                                        notifyPermChanged();
                                    }
                                    else if(UsersFragment.mHandler != null) {
                                        Message message = UsersFragment.mHandler.obtainMessage(UsersFragment.UPDATE);
                                        message.sendToTarget();
                                    }
                                    break;
                                case REMOVED : if(position >= 0) users.remove(position);
                                    if(UsersFragment.mHandler != null) {
                                        Message message = UsersFragment.mHandler.obtainMessage(UsersFragment.UPDATE);
                                        message.sendToTarget();
                                    }
                                    break;
                            }
                        }
                    }
                });
    }

    private void notifyPermChanged() {
        if(BikesFragment.mHandler != null) {
            Message message = BikesFragment.mHandler.obtainMessage(BikesFragment.PERMISSION, currentUser.getEmail());
            message.sendToTarget();
        }
        if(UsersFragment.mHandler != null) {
            Message message = UsersFragment.mHandler.obtainMessage(UsersFragment.UPDATE);
            message.sendToTarget();
        }
        if(MenuFragment.mHandler != null) {
            Message message = MenuFragment.mHandler.obtainMessage(MenuFragment.PERMISSION);
            message.sendToTarget();
        }
    }

    @Override
    public void onBackPressed() {
        if(onMenu) ;
        else {
            toolbar.setTitle(R.string.app_name);
            onMenu = true;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.animation_enter_left, R.anim.animation_leave_right)
                    .replace(R.id.frame_container, new MenuFragment())
                    .commit();
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
            //resume tasks needing this permission
        }
    }
}
