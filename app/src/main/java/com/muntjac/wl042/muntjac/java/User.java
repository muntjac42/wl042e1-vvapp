package com.muntjac.wl042.muntjac.java;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

/**
 * Created by guillaume on 14/03/18.
 */
@IgnoreExtraProperties
public class User implements Comparable<User> {

    private String name;
    private boolean staffLvl;

    @Exclude
    private String ID;

    public User() {
        this.name = "";
        this.staffLvl = false;
    }

    public User(String name, boolean staffLvl) {
        this.name = name;
        this.staffLvl = staffLvl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStaffLvl() {
        return staffLvl;
    }

    public void setStaffLvl(boolean staffLvl) {
        this.staffLvl = staffLvl;
    }

    @Exclude
    public String getID() {
        return ID;
    }

    @Exclude
    public void setID(String ID) {
        this.ID = ID;
    }

    public void update(User user) {
        this.name = user.name;
        this.staffLvl = user.staffLvl;
    }

    @Override
    public int compareTo(@NonNull User o) {
        return this.ID.compareTo(o.ID);
    }
}
