package com.muntjac.wl042.muntjac.activities;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.fragments.LoginFragment;
import com.muntjac.wl042.muntjac.fragments.RegisterFragment;

public class StartActivity extends AppCompatActivity {

    public static FirebaseAuth mAuth = FirebaseAuth.getInstance();
    LoginFragment loginFragment;
    RegisterFragment registerFragment;
    Fragment curr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        loginFragment = new LoginFragment();
        loginFragment.setStartActivity(this);

        registerFragment = new RegisterFragment();
        registerFragment.setStartActivity(this);

        curr = loginFragment;

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_container, curr).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        MainActivity.currentUser = mAuth.getCurrentUser();
        //if(account != null) System.out.println(account.getDisplayName());

        if(MainActivity.currentUser != null && MainActivity.currentUser.isEmailVerified()) {
            goToMainActivity();
        }
    }

    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Verifie si l'email est valide
     * @param email
     * @return
     */
    public boolean isEmailValid(String email) {
        boolean cond = email.contains("@");
        if(cond)
            cond = email.contains(".");
        if(cond)
            cond = email.length() >= 5;
        return cond;
    }

    /**
     * Verifie si le password est valide
     * @param password
     * @return
     */
    public boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }

    public void changeFragment() {
        if(curr.equals(loginFragment)) {
            curr = registerFragment;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.animation_enter_right, R.anim.animation_leave_left)
                    .replace(R.id.frame_container, curr)
                    .commit();
        } else {
            curr = loginFragment;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.animation_enter_left, R.anim.animation_leave_right)
                    .replace(R.id.frame_container, curr)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if(curr.equals(registerFragment)) {
            curr = loginFragment;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.animation_enter_left, R.anim.animation_leave_right)
                    .replace(R.id.frame_container, curr)
                    .commit();
        }
    }
}
