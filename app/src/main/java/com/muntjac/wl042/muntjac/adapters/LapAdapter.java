package com.muntjac.wl042.muntjac.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.java.Bike;
import com.muntjac.wl042.muntjac.java.Constants;
import com.muntjac.wl042.muntjac.java.Lap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.muntjac.wl042.muntjac.java.Constants.db;

/**
 * Created by guillaume on 11/03/18.
 */

public class LapAdapter extends ArrayAdapter<Lap> {

    Context context;
    boolean specialLvl;
    Bike bike;

    public LapAdapter(Context context, boolean specialLvl, Bike bike) {
        super(context, 0, bike.getDone());
        this.context = context;
        this.specialLvl = specialLvl;
        this.bike = bike;
    }

    public void setStaffLvl(boolean staffLvl) {
        this.specialLvl = staffLvl;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listed_lap, parent, false);
        }

        LapViewHolder viewHolder = (LapViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new LapViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.lap_name);
            viewHolder.duration = (TextView) convertView.findViewById(R.id.lap_duration);
            viewHolder.edit = (ImageButton) convertView.findViewById(R.id.edit_lap_name);
            viewHolder.delete = (ImageButton) convertView.findViewById(R.id.delete_lap);
            viewHolder.number = (TextView) convertView.findViewById(R.id.lap_number);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List
        final Lap lap = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.name.setText(lap.getName());
        viewHolder.duration.setText(context.getString(R.string.time) + " : " + lap.getFormatedDuration());
        viewHolder.number.setText(bike.getDone().size() - position + ".");
        if(!specialLvl) {
            viewHolder.edit.setVisibility(View.INVISIBLE);
            viewHolder.delete.setVisibility(View.INVISIBLE);
        }
        else {
            viewHolder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editLapName(lap.getName(), lap);
                }
            });
            viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteLap(lap);
                }
            });
        }

        return convertView;
    }

    private void editLapName(String name, final Lap lap) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View content = layoutInflater.inflate(R.layout.new_name, null);
        content.setPadding(15, 15, 15, 15);

        final EditText actualName = content.findViewById(R.id.user_name);
        actualName.setText(name);

        final AlertDialog.Builder password_dialog = new AlertDialog.Builder(getContext());
        password_dialog.setTitle(context.getString(R.string.edit_name))
                .setView(content)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.collection(Constants.COLLECTION_BIKES)
                                .document(bike.getID())
                                .collection(Constants.COLLECTION_DONE)
                                .document(lap.getID())
                                .update(Constants.FIELD_DONE_NAME, actualName.getText().toString().toUpperCase());
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        password_dialog.show();
    }

    private void deleteLap(final Lap lap) {
        TextView text = new TextView(context);
        text.setText(R.string.delete_lap_message);
        text.setPadding(10, 10, 10, 10);

        final AlertDialog.Builder password_dialog = new AlertDialog.Builder(getContext());
        password_dialog.setTitle(context.getString(R.string.delete_lap))
                .setView(text)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeLap(lap);
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        password_dialog.show();
    }

    private void removeLap(final Lap lap) {
        db.collection(Constants.COLLECTION_BIKES)
                .document(bike.getID())
                .collection(Constants.COLLECTION_DONE)
                .document(lap.getID())
                .delete();
    }

    private class LapViewHolder {
        public TextView name;
        public TextView duration;
        public ImageButton edit;
        public ImageButton delete;
        public TextView number;
    }
}
