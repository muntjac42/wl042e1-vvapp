package com.muntjac.wl042.muntjac.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.java.Bike;
import com.muntjac.wl042.muntjac.java.Constants;
import com.muntjac.wl042.muntjac.subfragments.TargetBikeFragment;
import com.muntjac.wl042.muntjac.subfragments.TargetPassageFragment;
import com.muntjac.wl042.muntjac.subfragments.TargetStatFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.muntjac.wl042.muntjac.activities.MainActivity.myBikes;
import static com.muntjac.wl042.muntjac.java.Constants.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class BikesFragment extends Fragment {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private View view;
    private TabLayout tabLayout;

    private Bike bike;
    private Fragment[] frags = new Fragment[3];

    public static Handler mHandler;
    public static final int REMOVED = 0;
    public static final int MODIFIED = 2;
    public static final int PERMISSION = 5;

    public BikesFragment() {
        // Required empty public constructor
    }

    public static BikesFragment newInstance(Bike bike) {
        BikesFragment frag = new BikesFragment();
        frag.bike = bike;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //if(MainActivity.user != null) setHasOptionsMenu(MainActivity.user.isStaffLvl());
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_bikes, container, false);

        frags[0] = TargetBikeFragment.newInstance(bike);
        frags[1] = TargetPassageFragment.newInstance(bike);
        frags[2] = TargetStatFragment.newInstance(bike);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        setUpHandler();

        return view;
    }

    private void setUpHandler() {
        mHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                String ID = (String) msg.obj;
                Bike mbike = Bike.newEmptyBike("");
                mbike.setID(ID);
                switch (msg.what) {
                    case REMOVED :
                        // TODO
                        break;
                    case MODIFIED : if(bike.equals(mbike) && frags != null) {
                        if(((TargetBikeFragment) frags[0]).mHandler != null) {
                            Message message = ((TargetBikeFragment) frags[0]).mHandler.obtainMessage(TargetBikeFragment.CHRONO);
                            message.sendToTarget();
                        }
                        if(((TargetPassageFragment) frags[1]).mHandler != null) {
                            Message message = ((TargetPassageFragment) frags[1]).mHandler.obtainMessage(TargetPassageFragment.UPDATE);
                            message.sendToTarget();
                        }
                        if(((TargetStatFragment) frags[2]).mHandler != null) {
                            Message message = ((TargetStatFragment) frags[2]).mHandler.obtainMessage(TargetStatFragment.UPDATE);
                            message.sendToTarget();
                        }
                    }
                        break;
                    case PERMISSION : if(bike.equals(mbike) && frags != null) {
                        if(((TargetBikeFragment) frags[0]).mHandler != null) {
                            Message message = ((TargetBikeFragment) frags[0]).mHandler.obtainMessage(TargetBikeFragment.PERMISSION);
                            message.sendToTarget();
                        }
                        if(((TargetPassageFragment) frags[1]).mHandler != null) {
                            Message message = ((TargetPassageFragment) frags[1]).mHandler.obtainMessage(TargetPassageFragment.PERMISSION);
                            message.sendToTarget();
                        }
                        if(((TargetStatFragment) frags[2]).mHandler != null) {
                            Message message = ((TargetStatFragment) frags[2]).mHandler.obtainMessage(TargetStatFragment.PERMISSION);
                            message.sendToTarget();
                        }
                    }
                        break;
                }
                return true;
            }
        });
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position >= frags.length) return null;
            return frags[position];
        }

        @Override
        public int getCount() {
            return frags.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0 : return getString(R.string.chrono);
                case 1 : return getString(R.string.order);
                case 2 : return getString(R.string.stats);
                default: return "";
            }
        }
    }
}
