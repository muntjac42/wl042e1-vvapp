package com.muntjac.wl042.muntjac.java;

import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Created by guillaume on 11/03/18.
 */

public class Constants {

    public static final FirebaseFirestore db = FirebaseFirestore.getInstance();

    public static final String COLLECTION_USERS = "users";
    public static final String FIELD_USERS_NAME = "name";
    public static final String FIELD_USERS_STAFFLVL = "staffLvl";

    public static final String COLLECTION_STAFF = "staff";
    public static final String DOCUMENT_STAFF_NAME = "name";
    public static final String DOCUMENT_STAFF_HOLDING = "holding";

    public static final String COLLECTION_LAPS = "laps";
    public static final String DOCUMENT_LAPS_CURRENT1 = "firstbike_0";
    public static final String DOCUMENT_LAPS_CURRENT2 = "secondbike_0";
    public static final String DOCUMENT_LAPS_BIKE1 = "firstbike";
    public static final String DOCUMENT_LAPS_BIKE2 = "secondbike";
    public static final String DOCUMENT_LAPS_START = "start";
    public static final String DOCUMENT_LAPS_END = "end";
    public static final String DOCUMENT_LAPS_NAME = "name";
    public static final String DOCUMENT_LAPS_RUNNING = "running";
    public static final String DOCUMENT_LAPS_BIKE = "bike";
    public static final String DOCUMENT_LAPS_COUNT = "laps";

    public static final String COLLECTION_PASSAGES = "passages";
    public static final String DOCUMENT_PASSAGES_CURRENT1 = "firstbike_0";
    public static final String DOCUMENT_PASSAGES_CURRENT2 = "secondbike_0";
    public static final String DOCUMENT_PASSAGES_BIKE1 = "firstbike";
    public static final String DOCUMENT_PASSAGES_BIKE2 = "secondbike";
    public static final String DOCUMENT_PASSAGES_NAME = "name";
    public static final String DOCUMENT_PASSAGES_ISNEXT = "isnext";
    public static final String DOCUMENT_PASSAGES_BIKE = "bike";
    public static final String DOCUMENT_PASSAGES_NEXT = "next";
    public static final String DOCUMENT_PASSAGES_COUNT = "passages";

    public static final String COLLECTION_NEWS = "news";
    public static final String DOCUMENT_NEWS_TITLE = "title";
    public static final String DOCUMENT_NEWS_CONTENT = "content";
    public static final String DOCUMENT_NEWS_DATE = "date";

    public static final String COLLECTION_BIKES = "bikes";
    public static final String FIELD_BIKES_BIKERS = "bikers";
    public static final String FIELD_BIKES_NAME = "name";
    public static final String FIELD_BIKES_COUNT = "count";
    public static final String FIELD_BIKES_AVERAGE = "average";
    public static final String FIELD_BIKES_BIKER_START = "bikerStart";
    public static final String FIELD_BIKES_RUNNING = "running";
    public static final String FIELD_BIKES_HOLDING = "holding";
    public static final String FIELD_BIKES_LAST_DONE = "lastDone";

    public static final String COLLECTION_DONE = "done";
    public static final String FIELD_DONE_NAME = "name";
    public static final String FIELD_DONE_TIME = "time";

    public static final String COLLECTION_PENDING = "pending";
    public static final String FIELD_PENDING_NAME = "name";
    public static final String FIELD_PENDING_NEXT = "next";
}
