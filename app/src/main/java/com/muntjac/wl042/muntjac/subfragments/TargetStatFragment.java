package com.muntjac.wl042.muntjac.subfragments;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.java.Bike;
import com.muntjac.wl042.muntjac.java.Lap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TargetStatFragment extends Fragment implements Comparable<TargetStatFragment> {

    private Bike bike;

    private View view;

    private TextView count;
    private TextView average;

    private TextView bestTimeName1;
    private TextView bestTimeValue1;
    private TextView bestTimeName2;
    private TextView bestTimeValue2;
    private TextView bestTimeName3;
    private TextView bestTimeValue3;

    private TextView bestPartName1;
    private TextView bestPartValue1;
    private TextView bestPartName2;
    private TextView bestPartValue2;
    private TextView bestPartName3;
    private TextView bestPartValue3;

    private TextView bestAverageName1;
    private TextView bestAverageValue1;
    private TextView bestAverageName2;
    private TextView bestAverageValue2;
    private TextView bestAverageName3;
    private TextView bestAverageValue3;

    public Handler mHandler;
    public static final int UPDATE = 0;
    public static final int PERMISSION = 1;

    public TargetStatFragment() {
        // Required empty public constructor
    }

    public static TargetStatFragment newInstance(Bike bike) {
        TargetStatFragment targetStatFragment = new TargetStatFragment();
        targetStatFragment.bike = bike;
        return targetStatFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_target_stat, container, false);

        count = view.findViewById(R.id.num_lap);
        average = view.findViewById(R.id.average_lap);

        bestTimeName1 = view.findViewById(R.id.time_1_name);
        bestTimeValue1 = view.findViewById(R.id.time_1_value);
        bestTimeName2 = view.findViewById(R.id.time_2_name);
        bestTimeValue2 = view.findViewById(R.id.time_2_value);
        bestTimeName3 = view.findViewById(R.id.time_3_name);
        bestTimeValue3 = view.findViewById(R.id.time_3_value);

        bestPartName1 = view.findViewById(R.id.part_1_name);
        bestPartValue1 = view.findViewById(R.id.part_1_value);
        bestPartName2 = view.findViewById(R.id.part_2_name);
        bestPartValue2 = view.findViewById(R.id.part_2_value);
        bestPartName3 = view.findViewById(R.id.part_3_name);
        bestPartValue3 = view.findViewById(R.id.part_3_value);

        bestAverageName1 = view.findViewById(R.id.average_1_name);
        bestAverageValue1 = view.findViewById(R.id.average_1_value);
        bestAverageName2 = view.findViewById(R.id.average_2_name);
        bestAverageValue2 = view.findViewById(R.id.average_2_value);
        bestAverageName3 = view.findViewById(R.id.average_3_name);
        bestAverageValue3 = view.findViewById(R.id.average_3_value);

        setUpView();
        setUpHandler();

        return view;
    }

    private void setUpHandler() {
        mHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case UPDATE : setUpView();
                        break;
                }
                return true;
            }
        });
    }

    private void setUpView() {
        count.setText(bike.getDone().size() + "");

        long total = 0;

        Long[] besttimesvalue = {new Long(Long.MAX_VALUE), new Long(Long.MAX_VALUE), new Long(Long.MAX_VALUE)};
        String[] besttimesname = new String[3];

        for(Lap lap : bike.getDone()) {
            total += lap.getDuration();

            Long dur = new Long(lap.getDuration());
            if(dur.compareTo(besttimesvalue[0]) < 0) {
                besttimesvalue[2] = besttimesvalue[1];
                besttimesname[2] = besttimesname[1];
                besttimesvalue[1] = besttimesvalue[0];
                besttimesname[1] = besttimesname[0];
                besttimesvalue[0] = dur;
                besttimesname[0] = lap.getName();
            } else if (dur.compareTo(besttimesvalue[1]) < 0) {
                besttimesvalue[2] = besttimesvalue[1];
                besttimesname[2] = besttimesname[1];
                besttimesvalue[1] = dur;
                besttimesname[1] = lap.getName();
            } else if (dur.compareTo(besttimesvalue[2]) < 0) {
                besttimesvalue[2] = dur;
                besttimesname[2] = lap.getName();
            }
        }

        average.setText(durationToString(total/Math.max(bike.getDone().size(), 1)));

        if(besttimesvalue[0].compareTo(new Long(Long.MAX_VALUE)) < 0) {
            bestTimeValue1.setText(durationToString(besttimesvalue[0].longValue()));
            bestTimeName1.setText(besttimesname[0]);
        }
        if(besttimesvalue[1].compareTo(new Long(Long.MAX_VALUE)) < 0) {
            bestTimeValue2.setText(durationToString(besttimesvalue[1].longValue()));
            bestTimeName2.setText(besttimesname[1]);
        }
        if(besttimesvalue[2].compareTo(new Long(Long.MAX_VALUE)) < 0) {
            bestTimeValue3.setText(durationToString(besttimesvalue[2].longValue()));
            bestTimeName3.setText(besttimesname[2]);
        }

        long[] bestaveragevalue = {Long.MAX_VALUE, Long.MAX_VALUE, Long.MAX_VALUE};
        String[] bestaveragename = new String[3];

        int[] bestparticipationvalue = {Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE};
        String[] bestparticipationname = new String[3];

        List<MultipleLaps> multipleLaps = getMultipleLaps(bike);
        for(MultipleLaps lap : multipleLaps) {
            long average = lap.getAverage();
            int count = lap.getCount();

            if(average < bestaveragevalue[2]) {
                bestaveragename[2] = lap.getName();
                bestaveragevalue[2] = average;
                if(average < bestaveragevalue[1]) {
                    swap(bestaveragevalue, 1, 2);
                    swap(bestaveragename, 1, 2);
                    if(average < bestaveragevalue[0]) {
                        swap(bestaveragename, 0, 1);
                        swap(bestaveragevalue, 0, 1);
                    }
                }
            }

            if(count > bestparticipationvalue[2]) {
                bestparticipationname[2] = lap.getName();
                bestparticipationvalue[2] = count;
                if(count > bestparticipationvalue[1]) {
                    swap(bestparticipationvalue, 1, 2);
                    swap(bestparticipationname, 1, 2);
                    if(count > bestparticipationvalue[0]) {
                        swap(bestparticipationname, 0, 1);
                        swap(bestparticipationvalue, 0, 1);
                    }
                }
            }
        }

        if(bestaveragevalue[0] < Long.MAX_VALUE) {
            bestAverageValue1.setText(durationToString(bestaveragevalue[0]));
            bestAverageName1.setText(bestaveragename[0]);
        }
        if(bestaveragevalue[1] < Long.MAX_VALUE) {
            bestAverageValue2.setText(durationToString(bestaveragevalue[1]));
            bestAverageName2.setText(bestaveragename[1]);
        }
        if(bestaveragevalue[2] < Long.MAX_VALUE) {
            bestAverageValue3.setText(durationToString(bestaveragevalue[2]));
            bestAverageName3.setText(bestaveragename[2]);
        }

        if(bestparticipationvalue[0] > Integer.MIN_VALUE) {
            bestPartValue1.setText(bestparticipationvalue[0] + "");
            bestPartName1.setText(bestparticipationname[0]);
        }
        if(bestparticipationvalue[1] > Integer.MIN_VALUE) {
            bestPartValue2.setText(bestparticipationvalue[1] + "");
            bestPartName2.setText(bestparticipationname[1]);
        }
        if(bestparticipationvalue[2] > Integer.MIN_VALUE) {
            bestPartValue3.setText(bestparticipationvalue[2] + "");
            bestPartName3.setText(bestparticipationname[2]);
        }
    }

    private void swap(int[] tab, int i, int j) {
        int tmp = tab[i];
        tab[i] = tab[j];
        tab[j] = tmp;
    }

    private void swap(long[] tab, int i, int j) {
        long tmp = tab[i];
        tab[i] = tab[j];
        tab[j] = tmp;
    }

    private void swap(String[] tab, int i, int j) {
        String tmp = tab[i];
        tab[i] = tab[j];
        tab[j] = tmp;
    }

    private List<MultipleLaps> getMultipleLaps(Bike bike) {
        List<MultipleLaps> laps = new ArrayList<>();
        if(bike.getDone().isEmpty()) return laps;
        for(Lap lap : bike.getDone()) {
            String s = lap.getName();
            if(laps.isEmpty()) laps.add(new MultipleLaps(1, lap.getDuration(), s));
            else for(int i = 0; i < laps.size(); i++) {
                MultipleLaps m = laps.get(i);
                if(m.getName().equals(s)) {
                    m.setAverage((m.getAverage()*m.getCount() + lap.getDuration())/(m.getCount() + 1));
                    m.setCount(m.getCount() + 1);
                    i = laps.size();
                } else if (i == laps.size() - 1) {
                    laps.add(new MultipleLaps(1, lap.getDuration(), s));
                    i = Integer.MAX_VALUE - 1;
                }
            }
        }

        //for(MultipleLaps l : laps) System.out.println(l.getName() + "\t : " + l.getAverage() + "\t : " + l.getCount());

        return laps;
    }

    private String durationToString(long duration) {
        int sec = ((int) duration/1000)%60;
        int min = ((int) duration/60000)%60;
        int h = ((int) duration/3600000)%24;

        return String.format("%02d:%02d:%02d", h, min, sec);
    }

    @Override
    public int compareTo(@NonNull TargetStatFragment o) {
        return this.bike.compareTo(o.bike);
    }

    private class MultipleLaps {

        private int count;
        private Long average;
        private String name;

        public MultipleLaps(int count, Long average, String name) {
            this.count = count;
            this.average = average;
            this.name = name;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public Long getAverage() {
            return average;
        }

        public void setAverage(Long average) {
            this.average = average;
        }

        public String getName() {
            return name;
        }
    }
}
