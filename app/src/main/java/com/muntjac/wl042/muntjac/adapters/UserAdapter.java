package com.muntjac.wl042.muntjac.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.java.Constants;
import com.muntjac.wl042.muntjac.java.User;

import java.util.List;

import static com.muntjac.wl042.muntjac.java.Constants.db;

/**
 * Created by guillaume on 14/03/18.
 */

public class UserAdapter extends ArrayAdapter<User> {

    private Context context;

    public UserAdapter(Context context, List<User> list) {
        super(context, 0, list);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listed_user, parent, false);
        }

        UserViewHolder viewHolder = (UserViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new UserViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.mail = (TextView) convertView.findViewById(R.id.mail);
            viewHolder.staff = (CheckBox) convertView.findViewById(R.id.staff);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List
        final User user = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.name.setText(user.getName());
        viewHolder.mail.setText(user.getID());
        viewHolder.staff.setChecked(user.isStaffLvl());

        if(!viewHolder.staff.isChecked() && MainActivity.user != null && MainActivity.user.isStaffLvl())
            viewHolder.staff.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((CheckBox)v).isChecked())
                        setStaffClicked(user);
                }
            });
        else viewHolder.staff.setClickable(false);

        return convertView;
    }

    private void setStaffClicked(final User user) {
        TextView text = new TextView(context);
        text.setText(R.string.confirm_make_staff);
        text.setPadding(10, 10, 10, 10);

        final AlertDialog.Builder password_dialog = new AlertDialog.Builder(getContext());
        password_dialog.setTitle(context.getString(R.string.add_staff))
                .setView(text)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.collection(Constants.COLLECTION_USERS)
                                .document(user.getID())
                                .update(Constants.FIELD_USERS_STAFFLVL, true);
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        password_dialog.show();
    }

    private class UserViewHolder {
        public TextView name;
        public TextView mail;
        public CheckBox staff;
    }
}
