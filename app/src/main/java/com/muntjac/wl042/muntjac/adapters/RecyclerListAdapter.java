package com.muntjac.wl042.muntjac.adapters;

/**
 * Created by guillaume on 19/03/18.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.java.Bike;
import com.muntjac.wl042.muntjac.java.Constants;

import java.util.Collections;
import java.util.List;

public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.LapViewHolder> implements ItemTouchHelperAdapter {

    private Bike bike;
    private boolean specialLvl;
    private Context context;

    private final OnStartDragListener mDragStartListener;

    public RecyclerListAdapter(Context context, boolean specialLvl, Bike bike, OnStartDragListener dragListener) {
        this.context = context;
        this.bike = bike;
        this.specialLvl = specialLvl;
        this.mDragStartListener = dragListener;
    }

    public void setStaffLvl(boolean staffLvl) {
        this.specialLvl = staffLvl;
    }

    @Override
    public LapViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listed_lap, parent, false);
        return new LapViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LapViewHolder viewHolder, final int position) {
        //getItem(position) va récupérer l'item [position] de la List
        final String s = bike.getBikers().get(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.name.setText(s);
        viewHolder.number.setText(bike.getDone().size() + 1 + position + ".");
        viewHolder.time.setVisibility(View.INVISIBLE);
        if(!specialLvl) {
            viewHolder.edit.setVisibility(View.INVISIBLE);
            viewHolder.delete.setVisibility(View.INVISIBLE);
        }
        else {
            viewHolder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    editLapName(s, position);
                }
            });
            viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deletePassage(position);
                }
            });
            viewHolder.number.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                        mDragStartListener.onStartDrag(viewHolder);
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if(!specialLvl) return;
        Collections.swap(bike.getBikers(), fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {

    }

    @Override
    public int getItemCount() {
        return bike.getBikers().size();
    }

    private void editLapName(String name, final int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View content = layoutInflater.inflate(R.layout.new_name, null);
        content.setPadding(15, 15, 15, 15);

        final EditText actualName = content.findViewById(R.id.user_name);
        actualName.setText(name);

        final AlertDialog.Builder password_dialog = new AlertDialog.Builder(context);
        password_dialog.setTitle(context.getString(R.string.edit_name))
                .setView(content)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        bike.getBikers().set(position, actualName.getText().toString().toUpperCase());
                        Constants.db.collection(Constants.COLLECTION_BIKES).document(bike.getID()).set(bike);
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        password_dialog.show();
    }

    private void deletePassage(final int position) {
        TextView text = new TextView(context);
        text.setText(R.string.delete_lap_message);
        text.setPadding(15, 15, 15, 15);

        final AlertDialog.Builder password_dialog = new AlertDialog.Builder(context);
        password_dialog.setTitle(context.getString(R.string.delete_lap))
                .setView(text)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        List<String> bikers = bike.getCopyOfBikers();
                        if(!bikers.isEmpty()) bikers.remove(position);
                        Constants.db.collection(Constants.COLLECTION_BIKES).document(bike.getID()).update(Constants.FIELD_BIKES_BIKERS, bikers);
                    }
                })
                .setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        password_dialog.show();
    }

    public class LapViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {

        public View view;

        public TextView name;
        public ImageButton edit;
        public ImageButton delete;
        public TextView number;
        public TextView time;

        public LapViewHolder(View v) {
            super(v);
            this.view = v;
            name = v.findViewById(R.id.lap_name);
            edit = v.findViewById(R.id.edit_lap_name);
            delete = v.findViewById(R.id.delete_lap);
            number = v.findViewById(R.id.lap_number);
            time = v.findViewById(R.id.lap_duration);
            time.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onItemSelected() {
            view.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            view.setBackgroundColor(0);
            if(specialLvl)
                Constants.db.collection(Constants.COLLECTION_BIKES).document(bike.getID()).update(Constants.FIELD_BIKES_BIKERS, bike.getBikers());
        }
    }
}