package com.muntjac.wl042.muntjac.java;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

/**
 * Created by guillaume on 11/03/18.
 */
@IgnoreExtraProperties
public class Lap implements Comparable<Lap> {

    private String name;
    private long start;
    private long end;

    @Exclude
    private String ID;

    public Lap() {
        this.name = "Baden";
        this.start = 0;
        this.end = end;
        this.ID = "42";
    }

    public Lap(String name, long start, long end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public long getStart() { return start; }

    public long getEnd() { return end; }

    @Exclude
    public long getDuration() {
        return end - start;
    }

    @Exclude
    public String getID() {
        return this.ID;
    }

    @Exclude
    public String getFormatedDuration() {
        int sec = (int)(getDuration()/1000)%60;
        int min = (int)(getDuration()/60000)%60;
        int hour = (int)(getDuration()/3600000)%24;
        return String.format("%02d:%02d:%02d", hour, min, sec);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    @Exclude
    public void setID(String ID) {
        this.ID = ID;
    }

    @Exclude
    public void update(Lap lap) {
        this.end = lap.end;
        this.start = lap.start;
        this.name = lap.name;
    }

    @Exclude
    public boolean equals(Lap l) {
        return this.ID == l.getID();
    }

    @Override
    public int compareTo(@NonNull Lap o) {
        return (int) (o.start - this.start);
    }

    public String toCsvString() {
        StringBuffer buf = new StringBuffer();
        buf.append(name);
        buf.append("," + getFormatedDuration());
        buf.append("," + getStart());
        buf.append("," + getEnd());
        buf.append("," + getDuration());
        buf.append("," + getID());
        return buf.toString();
    }
}
