package com.muntjac.wl042.muntjac.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.activities.StartActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment implements View.OnClickListener {

    private RelativeLayout firstBikeBtn;
    private RelativeLayout secondBikeBtn;
    private RelativeLayout newsBtn;
    private RelativeLayout accountBtn;
    private RelativeLayout logOutBtn;
    private RelativeLayout usersBtn;

    private TextView hello;

    private View view;

    public static Handler mHandler;
    public static final int PERMISSION = 0;
    public static final int BIKES = 1;

    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_menu, container, false);

        firstBikeBtn = view.findViewById(R.id.first_bike_btn);
        secondBikeBtn = view.findViewById(R.id.second_bike_btn);
        newsBtn = view.findViewById(R.id.news_btn);
        newsBtn.setOnClickListener(this);
        accountBtn = view.findViewById(R.id.account_btn);
        accountBtn.setOnClickListener(this);
        logOutBtn = view.findViewById(R.id.log_out_btn);
        logOutBtn.setOnClickListener(this);
        usersBtn = view.findViewById(R.id.manage_users_btn);
        hello = view.findViewById(R.id.hello_user);
        if(MainActivity.user != null)
        hello.setText("Hello " + MainActivity.user.getName());
        updateBikes();
        updatePerm();
        setUpHandler();

        return view;
    }

    private void updatePerm() {
        if(MainActivity.user != null)
            hello.setText("Hello " + MainActivity.user.getName());
        if(MainActivity.user != null && MainActivity.user.isStaffLvl()) {
            usersBtn.setOnClickListener(this);
            usersBtn.setVisibility(View.VISIBLE);
        } else {
            usersBtn.setVisibility(View.INVISIBLE);
        }
    }

    private void updateBikes() {
        if(MainActivity.myBikes != null && MainActivity.myBikes.size() > 0) {
            firstBikeBtn.setOnClickListener(this);
            firstBikeBtn.setVisibility(View.VISIBLE);
            if(MainActivity.myBikes.size() > 1) {
                secondBikeBtn.setOnClickListener(this);
                secondBikeBtn.setVisibility(View.VISIBLE);
            }
            else {
                secondBikeBtn.setVisibility(View.INVISIBLE);
            }
        }
        else {
            secondBikeBtn.setVisibility(View.INVISIBLE);
            firstBikeBtn.setVisibility(View.INVISIBLE);
        }
    }

    private void setUpHandler() {
        mHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case PERMISSION :
                        updatePerm();
                        break;
                    case BIKES :
                        updateBikes();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();
        Fragment fragment = null;

        switch (id) {
            case R.id.first_bike_btn :
                if(MainActivity.myBikes != null && MainActivity.myBikes.size() > 0) {
                    fragment = BikesFragment.newInstance(MainActivity.myBikes.get(0));
                    MainActivity.toolbar.setTitle(MainActivity.myBikes.get(0).getName());
                }
                break;
            case R.id.second_bike_btn :
                if(MainActivity.myBikes != null && MainActivity.myBikes.size() > 1) {
                    fragment = BikesFragment.newInstance(MainActivity.myBikes.get(1));
                    MainActivity.toolbar.setTitle(MainActivity.myBikes.get(1).getName());
                }
                break;
            case R.id.news_btn :
                fragment = new NewsFragment();
                MainActivity.toolbar.setTitle(R.string.news);
                break;
            case R.id.account_btn :
                fragment = new AccountFragment();
                MainActivity.toolbar.setTitle(R.string.account);
                break;
            case R.id.log_out_btn :
                logOut();
                break;
            case R.id.manage_users_btn :
                fragment = new UsersFragment();
                MainActivity.toolbar.setTitle(R.string.users);
                break;
        }

        if(fragment != null) {
            MainActivity.onMenu = false;
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.animation_enter_right, R.anim.animation_leave_left)
                    .replace(R.id.frame_container, fragment)
                    .commit();
        }
    }

    private void logOut() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getActivity(), StartActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
