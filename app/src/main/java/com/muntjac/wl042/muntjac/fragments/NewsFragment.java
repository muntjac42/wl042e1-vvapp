package com.muntjac.wl042.muntjac.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.java.Constants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.muntjac.wl042.muntjac.java.Constants.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {

    ListView listView;
    ArrayList<News> newsList;
    NewsAdapter adapter;

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(MainActivity.user != null) setHasOptionsMenu(MainActivity.user.isStaffLvl());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        listView = view.findViewById(R.id.list_news);
        listView.setClickable(false);

        newsList = new ArrayList<>();
        adapter = new NewsAdapter(getActivity(), newsList);
        listView.setAdapter(adapter);

        setNews();
        setOnNewNews();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);

        final MenuItem writeNews = menu.findItem(R.id.write);
        writeNews.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                writeNews();
                return true;
            }
        });
    }

    private void setNews() {
        db.collection(Constants.COLLECTION_NEWS)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()) {
                            for(DocumentSnapshot document : task.getResult()) {
                                if(document.exists()
                                        && document.contains(Constants.DOCUMENT_NEWS_TITLE)
                                        && document.contains(Constants.DOCUMENT_NEWS_CONTENT)
                                        && document.contains(Constants.DOCUMENT_NEWS_DATE)) {
                                    String title = (String) document.get(Constants.DOCUMENT_NEWS_TITLE);
                                    String content = (String) document.get(Constants.DOCUMENT_NEWS_CONTENT);
                                    long date = (long) document.get(Constants.DOCUMENT_NEWS_DATE);
                                    News n = new News(title, date, content, document.getId());
                                    newsList.add(n);
                                }
                            }
                            Collections.sort(newsList);
                            killPairs(newsList);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }

    private void killPairs(ArrayList<News> newsList) {
        if(newsList.isEmpty() || newsList.size() == 1) return;
        News n = newsList.get(0);
        for(int i = 1; i < newsList.size(); ) {
            if(n.equals(newsList.get(i))) newsList.remove(i);
            else i++;
            n = newsList.get(i - 1);
        }
    }

    private void setOnNewNews() {
        db.collection(Constants.COLLECTION_NEWS)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if(documentSnapshots == null) return;
                        for(DocumentSnapshot document : documentSnapshots) {
                            if(document.exists()
                                    && document.contains(Constants.DOCUMENT_NEWS_TITLE)
                                    && document.contains(Constants.DOCUMENT_NEWS_CONTENT)
                                    && document.contains(Constants.DOCUMENT_NEWS_DATE)) {
                                String title = (String) document.get(Constants.DOCUMENT_NEWS_TITLE);
                                String content = (String) document.get(Constants.DOCUMENT_NEWS_CONTENT);
                                long date = (long) document.get(Constants.DOCUMENT_NEWS_DATE);
                                newsList.add(new News(title, date, content, document.getId()));
                                Collections.sort(newsList);
                                killPairs(newsList);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    public void writeNews() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View content = layoutInflater.inflate(R.layout.new_news, null);

        final EditText title = content.findViewById(R.id.title);
        final EditText message = content.findViewById(R.id.content);

        final AlertDialog.Builder password_dialog = new AlertDialog.Builder(getContext());
        password_dialog.setTitle(getString(R.string.write_news))
                .setView(content)
                .setPositiveButton(getString(R.string.send_news), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendNews(new News(
                                title.getText().toString(),
                                System.currentTimeMillis(),
                                message.getText().toString(), "42")
                        );
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    public void sendNews(News news) {
        Map<String, Object> newNews = new HashMap<>();
        newNews.put(Constants.DOCUMENT_NEWS_TITLE, news.getTitle());
        newNews.put(Constants.DOCUMENT_NEWS_CONTENT, news.getMessage());
        newNews.put(Constants.DOCUMENT_NEWS_DATE, news.getDate());
        db.collection(Constants.COLLECTION_NEWS)
                .document()
                .set(newNews);
    }

    public class News implements Comparable<News> {
        private String title;
        private long date;
        private String message;
        private String ID;

        public News() {
            this.title = "Title";
            this.date = System.currentTimeMillis();
            this.message = "Message content example";
            this.ID = "42";
        }

        public News(String title, long date, String message, String ID) {
            this.title = title;
            this.date = date;
            this.message = message;
            this.ID = ID;
        }

        public String getTitle() {
            return title;
        }

        public long getDate() {
            return date;
        }

        public String getMessage() {
            return message;
        }

        public String getID() {
            return this.ID;
        }

        public boolean equals(News n) {
            return n.ID.equals(ID);
        }

        @Override
        public int compareTo(@NonNull News o) {
            return (int)(o.getDate() - this.date);
        }
    }

    public class NewsAdapter extends ArrayAdapter<News> {

        Context context;

        public NewsAdapter(Context context, List<News> newsList) {
            super(context, 0, newsList);
            this.context = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.listed_news,parent, false);
            }

            NewsViewHolder viewHolder = (NewsViewHolder) convertView.getTag();
            if(viewHolder == null){
                viewHolder = new NewsViewHolder();
                viewHolder.title = (TextView) convertView.findViewById(R.id.news_title);
                viewHolder.date = (TextView) convertView.findViewById(R.id.news_date);
                viewHolder.content = (TextView) convertView.findViewById(R.id.news_content);
                viewHolder.delete = (ImageButton) convertView.findViewById(R.id.delete);
                convertView.setTag(viewHolder);
            }

            //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
            final News news = getItem(position);

            //il ne reste plus qu'à remplir notre vue
            viewHolder.title.setText(news.getTitle());
            viewHolder.date.setText(new Date(news.getDate()).toString());
            viewHolder.content.setText(news.getMessage());
            viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(context)
                            .setMessage(context.getString(R.string.delete_news_message))
                            .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    deleteNews(news);
                                }
                            })
                            .setNegativeButton(context.getString(R.string.no), null)
                            .show();
                }
            });
            if(MainActivity.user == null || !MainActivity.user.isStaffLvl()) viewHolder.delete.setVisibility(View.INVISIBLE);

            return convertView;
        }

        private void deleteNews(News news) {
            db.collection(Constants.COLLECTION_NEWS).document(news.getID()).delete();
            newsList.remove(news);
            adapter.notifyDataSetChanged();
        }

        private class NewsViewHolder {
            public TextView title;
            public TextView date;
            public TextView content;
            public ImageButton delete;
        }
    }
}
