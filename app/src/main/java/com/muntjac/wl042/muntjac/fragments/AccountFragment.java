package com.muntjac.wl042.muntjac.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.activities.StartActivity;
import com.muntjac.wl042.muntjac.java.Constants;
import com.muntjac.wl042.muntjac.java.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.muntjac.wl042.muntjac.java.Constants.db;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment implements View.OnClickListener {

    Button changePassword;
    Button deleteAccount;
    EditText username;
    ImageButton saveName;

    private static final String TAG = "AccountFragment";

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        changePassword = view.findViewById(R.id.change_password_button);
        changePassword.setOnClickListener(this);

        deleteAccount = view.findViewById(R.id.delete_account_button);
        deleteAccount.setOnClickListener(this);

        username = view.findViewById(R.id.user_name);
        username.setText(MainActivity.currentUser.getDisplayName());

        saveName = view.findViewById(R.id.save_name);
        saveName.setOnClickListener(this);

        TextView mail = view.findViewById(R.id.mail_textview);
        mail.setText(MainActivity.currentUser.getEmail());

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.change_password_button) {
            changePassword();
        } else if(id == R.id.delete_account_button) {
            deleteAccountClicked();
        } else if(id == R.id.save_name) {
            changeName();
        }
    }

    private void changeName() {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(username.getText().toString())
                .build();

        MainActivity.currentUser.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                        }
                    }
                });

        Constants.db.collection(Constants.COLLECTION_USERS)
                .document(MainActivity.currentUser.getEmail())
                .update(Constants.FIELD_USERS_NAME, username.getText().toString());
    }

    private void changePassword() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View content = layoutInflater.inflate(R.layout.new_password, null);

        final AlertDialog.Builder password_dialog = new AlertDialog.Builder(getContext());
        password_dialog.setTitle(getString(R.string.change_password));
        password_dialog.setView(content);
        password_dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        password_dialog.show();

        final TextView success_message = (TextView) content.findViewById(R.id.success_msg);
        final EditText new_password = (EditText) content.findViewById(R.id.new_password);
        final EditText confirm_new_password = (EditText) content.findViewById(R.id.confirm_new_password);
        Button change_password = (Button) content.findViewById(R.id.new_password_btn);
        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean error = false;

                new_password.setError(null);
                confirm_new_password.setError(null);

                if(TextUtils.isEmpty(new_password.getText().toString())) {
                    error = true;
                    new_password.setError(getString(R.string.register_field_required));
                }
                else if(TextUtils.isEmpty(confirm_new_password.getText().toString())){
                    error = true;
                    confirm_new_password.setError(getString(R.string.register_field_required));
                }
                else if(new_password.getText().toString().length() < 6) {
                    error = true;
                    new_password.setError(getString(R.string.register_too_short_password));
                }
                else if(!new_password.getText().toString().equals(confirm_new_password.getText().toString())){
                    error = true;
                    confirm_new_password.setError(getString(R.string.register_different_password));
                }

                if(!error) {
                    updatePassword(new_password.getText().toString());

                    success_message.setText(getString(R.string.settings_succes_new_password));
                }
                else {
                    success_message.setText("");
                }

            }
        });
    }

    private void updatePassword(String password) {
        MainActivity.currentUser.updatePassword(password)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User password updated.");
                        }
                    }
                });
    }

    private void deleteAccountClicked() {
        new AlertDialog.Builder(getActivity())
                .setMessage(getString(R.string.delete_account_message))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteAccount();
                    }
                })
                .setNegativeButton(getString(R.string.no), null)
                .show();
    }

    private void deleteAccount() {
        FirebaseAuth.getInstance().getCurrentUser().delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User account deleted.");
                            if(MainActivity.user != null)
                                db.collection(Constants.COLLECTION_USERS).document(MainActivity.user.getID()).delete();
                            Intent intent = new Intent(getActivity(), StartActivity.class);
                            getActivity().finish();
                            startActivity(intent);
                        }
                    }
                });
    }
}
