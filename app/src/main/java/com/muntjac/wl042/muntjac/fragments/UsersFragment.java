package com.muntjac.wl042.muntjac.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.adapters.UserAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsersFragment extends Fragment {

    private View view;
    private ListView list;
    private UserAdapter adapter;

    public static Handler mHandler;
    public static final int UPDATE = 0;

    public UsersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(false);
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_users, container, false);

        list = (ListView) view.findViewById(R.id.users_list);
        adapter = new UserAdapter(getContext(), MainActivity.users);
        list.setAdapter(adapter);

        setUpHandler();

        return view;
    }

    private void setUpHandler() {
        mHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case UPDATE : adapter.notifyDataSetChanged();
                        break;
                    default : adapter.notifyDataSetChanged();
                        break;
                }
                return true;
            }
        });
    }
}
