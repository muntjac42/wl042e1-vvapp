package com.muntjac.wl042.muntjac.subfragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.adapters.LapAdapter;
import com.muntjac.wl042.muntjac.java.Bike;
import com.muntjac.wl042.muntjac.java.Constants;
import com.muntjac.wl042.muntjac.java.Lap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TargetBikeFragment extends Fragment implements View.OnClickListener, Comparable<TargetBikeFragment> {

    private static final String TAG = "TargetBikeFragment";
    private Bike bike;

    private Chronometer chronometer;
    private TextView biker;
    private TextView nextBiker;
    private TextView nextNextBiker;
    private Button start;
    private Button lap;
    private Button request;
    private ListView lapsView;
    private LapAdapter adapter;

    private DocumentReference documentReference;

    public Handler mHandler;
    public static final int CHRONO = 0;
    public static final int PERMISSION = 3;

    private static final String albumname = "Muntjac";
    private static final String filename = "lapsexport.csv";

    public TargetBikeFragment() {
        // Required empty public constructor
    }

    public static TargetBikeFragment newInstance(Bike bike) {
        TargetBikeFragment frag = new TargetBikeFragment();
        frag.bike = bike;
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_target_bike, container, false);

        boolean b = false;
        if(MainActivity.user != null && MainActivity.user.isStaffLvl()) b = true;

        chronometer = (Chronometer) view.findViewById(R.id.chrono);
        biker = (TextView) view.findViewById(R.id.biker_textview);
        nextBiker = (TextView) view.findViewById(R.id.next_biker_textview);
        nextNextBiker = (TextView) view.findViewById(R.id.next_next_biker_textview);
        start = (Button) view.findViewById(R.id.start_stop_button);
        lap = (Button) view.findViewById(R.id.lap_button);
        request = (Button) view.findViewById(R.id.request_button);
        adapter = new LapAdapter(getContext(), b, bike);
        updatePermView();
        lapsView = (ListView) view.findViewById(R.id.last_laps_listview);
        lapsView.setAdapter(adapter);

        documentReference = Constants.db.collection(Constants.COLLECTION_BIKES).document(bike.getID());

        updateChronoView();
        setUpHandler();

        return view;
    }

    private void setUpHandler() {
        mHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case CHRONO : updateChronoView();
                        adapter.notifyDataSetChanged();
                        break;
                    case PERMISSION :
                        updatePermView();
                        break;
                    default : adapter.notifyDataSetChanged();
                        updateChronoView();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if(id == R.id.start_stop_button) {
            if(!request.getText().equals(getString(R.string.holding))) showNoPerm();
            else if(start.getText().equals(getString(R.string.start))) start();
            else stop();
        }
        else if(id == R.id.request_button) {
            if(!request.getText().equals(getString(R.string.holding))) request();
        }
        else if(id == R.id.lap_button) {
            if(!request.getText().equals(getString(R.string.holding))) showNoPerm();
            else if(!start.getText().equals(getString(R.string.start))) {
                stop();
                start();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.bike_menu, menu);

        final MenuItem writeNews = menu.findItem(R.id.write);
        writeNews.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                writeLapsToFile();
                return true;
            }
        });
    }

    private void showNoPerm() {
        Snackbar.make(lapsView, getString(R.string.ask_modif), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.request), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        request();
                    }
                }).show();
    }

    private void start() {
        bike.setBikerStart(System.currentTimeMillis());
        bike.setRunning(true);

        documentReference.set(bike);
    }

    private void stop() {
        Lap lap = new Lap(biker.getText().toString(), bike.getBikerStart(), System.currentTimeMillis());
        addLap(lap);

        if(!bike.getBikers().isEmpty()) bike.getBikers().remove(0);
        bike.setRunning(false);
        documentReference.set(bike);
    }

    private void addLap(final Lap lap) {
        documentReference.collection(Constants.COLLECTION_DONE)
                .document()
                .set(lap);
    }

    private void request() {
        documentReference.update(Constants.FIELD_BIKES_HOLDING, MainActivity.currentUser.getEmail());
    }

    private void updatePermView() {
        if(MainActivity.user == null || !MainActivity.user.isStaffLvl()) {
            start.setVisibility(View.INVISIBLE);
            lap.setVisibility(View.INVISIBLE);
            request.setVisibility(View.INVISIBLE);
            adapter.setStaffLvl(false);
            adapter.notifyDataSetChanged();
        }
        else {
            start.setVisibility(View.VISIBLE);
            lap.setVisibility(View.VISIBLE);
            request.setVisibility(View.VISIBLE);
            start.setOnClickListener(this);
            lap.setOnClickListener(this);
            request.setOnClickListener(this);
            adapter.setStaffLvl(true);
            adapter.notifyDataSetChanged();
        }
    }

    private void updateChronoView() {
        if(bike.getBikers().size() > 0)
            biker.setText(bike.getBikers().get(0));
        else
            biker.setText("Unknown");
        if(bike.getBikers().size() > 1)
            nextBiker.setText(bike.getBikers().get(1));
        else
            nextBiker.setText(" -");
        if(bike.getBikers().size() > 2)
            nextNextBiker.setText(bike.getBikers().get(2));
        else
            nextNextBiker.setText("  -");

        if (bike.isRunning()) {
            chronometer.start();
            start.setText(R.string.stop);
            chronometer.setBase(SystemClock.elapsedRealtime()
                    - System.currentTimeMillis()
                    + bike.getBikerStart()
            );
        } else {
            chronometer.stop();
            start.setText(R.string.start);
            chronometer.setBase(SystemClock.elapsedRealtime());
        }

        if (bike.getHolding().equals(MainActivity.currentUser.getEmail()))
            request.setText(R.string.holding);
        else
            request.setText(R.string.request);
    }

    private void writeLapsToFile() {
        if(!isStoragePermissionGranted()) {
            Log.e(TAG, "Not sufficient permissions");
            return;
        }
        if(!isExternalStorageWritable()) return;
        //File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), albumname);
        //if (!dir.exists() || !dir.mkdirs())
        //    Log.e("Creating directory", "Directory not created");
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), bike.getName().toLowerCase() + " " + filename);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
        try {
            for(Lap l : bike.getDone())
                bufferedWriter.write(l.toCsvString() + "\n");
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Toast.makeText(getContext(), "File successfully written : " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {

                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

    @Override
    public int compareTo(@NonNull TargetBikeFragment o) {
        return this.bike.compareTo(o.bike);
    }
}
