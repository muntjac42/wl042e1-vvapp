package com.muntjac.wl042.muntjac.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.activities.StartActivity;

public class RegisterFragment extends Fragment {

    private View view;
    private StartActivity activity;

    private EditText name;
    private EditText email;
    private EditText password;
    private EditText confirm_password;
    private Button register;

    public static int PASSWORD_MIN_SIZE = 6;

    private final String TAG = "RegisterActivity";

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public RegisterFragment() {

    }

    public void setStartActivity(StartActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_register, container, false);

        name = (EditText) view.findViewById(R.id.register_name);
        email = (EditText) view.findViewById(R.id.register_email);
        password = (EditText) view.findViewById(R.id.register_password);
        confirm_password = (EditText) view.findViewById(R.id.register_confirm_password);

        register = (Button) view.findViewById(R.id.register_button);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(attemptRegister()) {
                    mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                            .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        // Sign in success, update UI with the signed-in user's information
                                        Log.d(TAG, "createUserWithEmail:success");
                                        MainActivity.currentUser = mAuth.getCurrentUser();

                                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                                .setDisplayName(name.getText().toString())
                                                .build();

                                        MainActivity.currentUser.updateProfile(profileUpdates)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Log.d(TAG, "User profile updated.");
                                                        }
                                                    }
                                                });

                                        sendVerification();

                                        activity.changeFragment();
                                    } else {
                                        // If sign in fails, display a message to the user.
                                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                        if(getActivity() != null)
                                        Toast.makeText(getActivity(), "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });

        return view;
    }

    private void sendVerification() {
        MainActivity.currentUser.sendEmailVerification()
                .addOnCompleteListener(activity, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {

                        if (task.isSuccessful()) {
                            Toast.makeText(activity,
                                    "Verification email sent to " + MainActivity.currentUser.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(activity,
                                    "Failed to send verification email.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /**
     * Methode de verification des paramètres entrés
     * Si tout est OK, return true, sinon false
     */
    private boolean attemptRegister() {
        //Reset error
        name.setError(null);
        email.setError(null);
        password.setError(null);
        confirm_password.setError(null);
        boolean error = false;

        if(TextUtils.isEmpty(name.getText().toString())){
            error = true;
            name.setError(getString(R.string.register_field_required));
        }

        if(TextUtils.isEmpty(email.getText().toString())){
            error = true;
            email.setError(getString(R.string.register_field_required));
        }
        else if(!isEmailValid(email.getText().toString())){
            error = true;
            email.setError(getString(R.string.register_invalid_email));
        }

        if(TextUtils.isEmpty(password.getText().toString())){
            error = true;
            password.setError(getString(R.string.register_field_required));
        }
        else if(TextUtils.isEmpty(confirm_password.getText().toString())){
            error = true;
            confirm_password.setError(getString(R.string.register_field_required));
        }
        else if(password.getText().toString().length() < PASSWORD_MIN_SIZE) {
            error = true;
            password.setError(getString(R.string.register_too_short_password));
        }
        else if(!password.getText().toString().equals(confirm_password.getText().toString())){
            error = true;
            confirm_password.setError(getString(R.string.register_different_password));
        }

        return !error;
    }

    /**
     * Methode de verification de la validité du mail entré en argument
     * @param email != null
     * @return true si le mail est valide, false sinon
     */
    private boolean isEmailValid(String email) {
        boolean cond = email.contains("@");
        if(cond)
            cond = email.contains(".");
        if(cond)
            cond = email.length() >= 5;
        return cond;
    }
}
