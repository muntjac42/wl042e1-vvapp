package com.muntjac.wl042.muntjac.java;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;
import com.muntjac.wl042.muntjac.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guillaume on 11/03/18.
 */
@IgnoreExtraProperties
public class Bike implements Comparable<Bike> {

    private String name;
    private String holding;
    private List<String> bikers;
    private boolean running;
    private long bikerStart;

    @Exclude
    private String ID;

    @Exclude
    private ArrayList<Lap> done = new ArrayList<>();

    public Bike() {
        this.name = "";
        this.holding = "";
        this.bikers = new ArrayList<>();
        this.running = false;
        this.bikerStart = 0;
    }

    public Bike(String name, String holding, List<String> bikers, boolean running, long bikerStart) {
        this.name = name;
        this.holding = holding;
        this.bikers = bikers;
        this.running = running;
        this.bikerStart = bikerStart;
    }

    public static Bike newEmptyBike(String name) {
        String mail = "unknown";
        if(MainActivity.currentUser != null) mail = MainActivity.currentUser.getEmail();
        Bike bike = new Bike(name,
                mail,
                new ArrayList<String>(),
                false,
                0);
        return bike;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHolding() {
        return holding;
    }

    public void setHolding(String holding) {
        this.holding = holding;
    }

    public List<String> getBikers() {
        return bikers;
    }

    public void setBikers(List<String> bikers) {
        this.bikers = bikers;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public long getBikerStart() {
        return bikerStart;
    }

    public void setBikerStart(long biker_start) {
        this.bikerStart = biker_start;
    }

    @Exclude
    public List<String> getCopyOfBikers() {
        List<String> list = new ArrayList<>();
        list.addAll(bikers);
        return list;
    }

    @Exclude
    public String getID() {
        return ID;
    }

    @Exclude
    public void setID(String ID) {
        this.ID = ID;
    }

    @Exclude
    public ArrayList<Lap> getDone() {
        return done;
    }

    @Exclude
    public void setDone(ArrayList<Lap> done) {
        this.done = done;
    }

    @Exclude
    public void update(Bike bike) {
        this.name = bike.name;
        this.running = bike.running;
        this.bikerStart = bike.bikerStart;
        this.bikers = bike.bikers;
        this.holding = bike.holding;
        this.ID = bike.ID;
    }

    @Exclude
    public boolean equals(Bike b) {
        return this.ID.equals(b.getID());
    }

    @Override
    public int compareTo(@NonNull Bike o) {
        return this.ID.compareTo(o.getID());
    }
}
