package com.muntjac.wl042.muntjac.subfragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.muntjac.wl042.muntjac.R;
import com.muntjac.wl042.muntjac.activities.MainActivity;
import com.muntjac.wl042.muntjac.adapters.OnStartDragListener;
import com.muntjac.wl042.muntjac.adapters.RecyclerListAdapter;
import com.muntjac.wl042.muntjac.adapters.SimpleItemTouchHelperCallback;
import com.muntjac.wl042.muntjac.java.Bike;
import com.muntjac.wl042.muntjac.java.Constants;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TargetPassageFragment extends Fragment implements Comparable<TargetPassageFragment>, OnStartDragListener {

    private View view;
    private RecyclerView recyclerView;
    private RecyclerListAdapter adapter;
    private Bike bike;

    public Handler mHandler;
    public static final int UPDATE = 0;
    public static final int PERMISSION = 1;

    private ItemTouchHelper mItemTouchHelper;

    public TargetPassageFragment() {
        // Required empty public constructor
    }

    public static TargetPassageFragment newInstance(Bike bike) {
        TargetPassageFragment targetPassageFragment = new TargetPassageFragment();
        targetPassageFragment.bike = bike;
        return targetPassageFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(MainActivity.user != null) setHasOptionsMenu(MainActivity.user.isStaffLvl());
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_target_passage, container, false);

        boolean b = false;
        if(MainActivity.user != null && MainActivity.user.isStaffLvl()) b = true;

        recyclerView = (RecyclerView) view.findViewById(R.id.passage_list);
        adapter = new RecyclerListAdapter(getContext(), b, bike, this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);

        setUpHandler();

        return view;
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    private void setUpHandler() {
        mHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case UPDATE :
                        boolean b = false;
                        if(MainActivity.user != null && MainActivity.user.isStaffLvl()) b = true;
                        adapter.setStaffLvl(b);
                        adapter.notifyDataSetChanged();
                        break;
                    case PERMISSION :
                        b = false;
                        if(MainActivity.user != null && MainActivity.user.isStaffLvl()) b = true;
                        adapter.setStaffLvl(b);
                        adapter.notifyDataSetChanged();
                        setHasOptionsMenu(MainActivity.user.isStaffLvl());
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);

        final MenuItem writeNews = menu.findItem(R.id.write);
        writeNews.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                addPassageClicked();
                return true;
            }
        });
    }

    private void addPassageClicked() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View content = inflater.inflate(R.layout.new_passage, null);

        final EditText editText = (EditText) content.findViewById(R.id.name);

        final AlertDialog.Builder password_dialog = new AlertDialog.Builder(getContext());
        password_dialog.setTitle(getContext().getString(R.string.add_passage_message))
                .setView(content)
                .setPositiveButton(getContext().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        List<String> bikers = bike.getCopyOfBikers();
                        bikers.add(editText.getText().toString().toUpperCase());
                        Constants.db.collection(Constants.COLLECTION_BIKES).document(bike.getID()).update(Constants.FIELD_BIKES_BIKERS, bikers);
                    }
                })
                .setNegativeButton(getContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        password_dialog.show();
    }

    @Override
    public int compareTo(@NonNull TargetPassageFragment o) {
        return this.bike.compareTo(o.bike);
    }
}
